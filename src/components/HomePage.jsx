import React from "react";
import TableShow from "./TableShow";
import { Button } from "react-bootstrap";
import {Link} from 'react-router-dom';

export default function AdminPage(props) {
  if (props.loading) {
    return <center>Loading...</center>;
  }
  return (
    <div  className="container">
      <center>
        <h1>Article Management</h1>
      </center>
      <center>
        <Button variant="dark" as={Link} to="/add">Add New Article</Button>
      </center> 
      <br />
      <TableShow data={props.data} onDelete={props.onDelete}/>
    </div>
  );
}
