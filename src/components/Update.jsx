import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import Axios from "axios";
import { Link } from "react-router-dom";
import Image from "../img/Default_Image_Thumbnail.png"


class UpdatePage extends Component {
  constructor(props) {
    super(props);
    var dataView = this.props.data.find(
      (d) => d.ID == this.props.match.params.id
    );
    this.state ={
      TITLE: dataView.TITLE,
      DESCRIPTION: dataView.DESCRIPTION,
      IMAGE: dataView.IMAGE,
    };
    console.log(this.state.IMAGE);
  }
  // onImageChange = (event) => {
  //   if (event.target.files && event.target.files[0]) {
  //     this.setState({
  //       IMAGE: URL.createObjectURL(event.target.files[0]),
  //     });
  //   }
  // };
  onImageChange = (event) => {
    console.log(event);
    if (event.target.files && event.target.files[0]) {
      this.setState({
        IMAGE: URL.createObjectURL(event.target.files[0]),
      });
    }
  };
  changeHandler = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  onUpdate = (updateID) => {
    if(this.state.TITLE===""|this.state.DESCRIPTION===""|this.state.IMAGE==="")
    {
      alert("TITLE , DESCRIPTION , IMAGE Cannot Null: Input Again");
          return;
    }
    else if(this.state.TITLE===""|this.state.DESCRIPTION===""|this.state.IMAGE==="")
    {
      alert("TITLE , DESCRIPTION , IMAGE Cannot Null: Input Again");
          return;
    }
    else{
    console.log(updateID);
    Axios.put(`http://110.74.194.124:15011/v1/api/articles/${updateID}`,
      this.state
    )
      .then((res) => {
        alert(res.data.MESSAGE);
        this.props.history.push("/");
      })
      .catch((err) => alert(err));
    }
  };
  render() {
    return (
      <div className="container ">
        <h1>Update Article</h1>
        <div className="row">
          <div className="col-lg-8">
            <Form>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Title</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Title"
                  name="TITLE"
                  value={this.state.TITLE}
                  onChange={this.changeHandler}
                />
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Description"
                  name="DESCRIPTION"
                  value={this.state.DESCRIPTION}
                  onChange={this.changeHandler}
                />
              </Form.Group>
            </Form>
          </div>
          <div
            className="col-lg-4"
            style={{ border: "1px solid #ccc", width: "150px" }}
          >
            <input
              type="file"
              style={{ opacity: "0", width: "100%" }}
              onChange={(e) => this.onImageChange(e)}
              className="filetype"
              id="group_image"
            />
            <img
              src={this.state.IMAGE}
              alt=""
              style={{ width: "100%", height: "88%" }}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <Button
              variant="primary"
              type="button"
              onClick={() => this.onUpdate(this.props.match.params.id)}
            >
              Update
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default UpdatePage;