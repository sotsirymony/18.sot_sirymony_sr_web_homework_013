import React, { Component } from "react";
import Axios from "axios";
import HomePage from "./components/HomePage";
import Navbarr from "./components/Navbarr";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import ShowView from "./components/View"
import UpdatePage from "./components/Update.jsx"
import FormAdd from "./components/FormAdd";



export default class App extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      data: [],
      isUpdate:false,
    };
  }
  componentDidMount() {
    Axios.get("http://110.74.194.124:15011/v1/api/articles")
      .then((res) =>
        this.setState({
          loading: false,
          data: res.data.DATA,
        })
      )
      .catch((err) => {
        alert(err)
      });
    console.log(this.state.data);
  }
  onUpdate=()=>{
    this.setState({
      isUpdate:true,
    })
  }
  onDelete=(deleteID)=>{
    let datas = this.state.data.filter((data) => data.ID !== deleteID);
    this.setState({
      data:datas,
    })
  }
  onAdd=()=>{
    Axios.get("http://110.74.194.124:15011/v1/api/articles")
      .then((res) =>
        this.setState({
          loading: false,
          data: res.data.DATA,
        })
      )
      .catch((err) => {
        alert(err);
      });
      console.log(this.state.data)
  }
  render() {
    return (
      <div>
        <Router>
          <Navbarr></Navbarr>
          <Switch>
           
          <Route path="/add" render={()=><FormAdd onAdd={this.onAdd}/>}/>
            <Route path={"/view/:id"}  render={(props)=><ShowView {...props} data={this.state.data}/>}/>
            <Route path={"/update/:id"}

              render={(props) => (
                  <UpdatePage
                  {...props}
                  data={this.state.data}
                />
              )}
            />
            
            <HomePage data={this.state.data} loading={this.state.loading} onDelete={this.onDelete} onAdd={this.onAdd}/>
          </Switch>
        </Router>
      </div>
    );
  }
}
